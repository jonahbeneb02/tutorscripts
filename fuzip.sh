#! /bin/sh

dir=$1
if [ $# -lt 2 ] || [ ! -f "$dir/grades.csv" ]; then
    printf "Usage: %s <directory> <pattern1> [pattern2] ...\n" $(basename $0) >&2
    printf "Supply a pattern matching files to be added to the zip, e.g. '*.pdf'\n" >&2
    printf "Execute within the directory containing 'grades.csv'\n" >&2
    exit 1
fi

while [ $# -ne 1 ]; do
    pattern=$2
    find "$dir" \
        -name "$pattern" \
        -print \
        | zip -9 "$dir" -@
    shift
done
