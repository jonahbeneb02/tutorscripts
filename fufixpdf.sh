#! /bin/sh

if [ $# -ne 0 ] || [ ! -f 'grades.csv' ]; then
    printf "Usage: %s\n" $(basename $0) >&2
    printf "Execute within the directory containing 'grades.csv'\n" >&2
    exit 1
fi

find \
    -wholename "*/Feedback Attachment(s)/*.pdf" \
    -exec sh -c 'echo fixing "$1" 2>&1; fixpdf "$1" "$1_fixed.pdf"; mv "$1_fixed.pdf" "$1";' sh {} \;
